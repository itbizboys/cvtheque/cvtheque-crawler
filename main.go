package main

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
	"github.com/emersion/go-message"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var mongoURI = "mongodb://root:Azer1234@cvtheque-db:27017/?retryWrites=true&w=majority"
var dbName = "cvtheque-db"
var filesPath = "./static/"

// var mongoURI = "mongodb+srv://itbiz:BizBoyz@itbiz.xxcrb.mongodb.net/bestptofile-cvtheque?retryWrites=true&w=majority"
// var dbName = "bestptofile-cvtheque"
// var filesPath = "../static/"

func main() {
	mailBoxes := getMailboxes()
	for i := 0; i < len(mailBoxes); i++ {
		crawler(mailBoxes[i]["imp_server"].(string), mailBoxes[i]["imp_port"].(int32), mailBoxes[i]["imp_login"].(string), mailBoxes[i]["imp_password"].(string))
	}
}

func mongoCon() {
	clientOptions := options.Client().ApplyURI(mongoURI)
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		fmt.Println(err)
	}

	err = client.Ping(context.TODO(), nil)

	if err != nil {
		fmt.Println(err)
	}

	log.Println("Connected to MongoDB!")

	// Don't forget to logout
	defer client.Disconnect(context.TODO())
}

// GetMailboxes Returns All Mail Boxes Recorded in the database
func getMailboxes() []bson.M {
	clientOptions := options.Client().ApplyURI(mongoURI)
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		fmt.Println(err)
	}

	err = client.Ping(context.TODO(), nil)

	if err != nil {
		fmt.Println(err)
	}

	log.Println("Connected to MongoDB!")

	// Don't forget to logout
	defer client.Disconnect(context.TODO())
	collection := client.Database(dbName).Collection("mailboxes")

	cursor, err := collection.Find(context.TODO(), bson.D{})
	if err != nil {
		fmt.Println(err)
	}

	defer cursor.Close(context.TODO())
	var mailBoxes []bson.M
	if err = cursor.All(context.TODO(), &mailBoxes); err != nil {
		fmt.Println(err)
	}
	return mailBoxes
}

// Crawler will connect to the given mail box and crawl attachments
func crawler(imapSRV string, imapPORT int32, imapLOGIN string, imapPASS string) {
	log.Println("Connecting to servers...")

	// Connect to server
	c, err := client.DialTLS(imapSRV+":"+strconv.Itoa(int(imapPORT)), nil)
	if err != nil {
		fmt.Println(err)
	}
	log.Println("Connected to imap server " + imapSRV)

	// Login
	if err := c.Login(imapLOGIN, imapPASS); err != nil {
		fmt.Println(err)
	}
	log.Println("Logged in to imap Server")

	clientOptions := options.Client().ApplyURI(mongoURI)
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		fmt.Println(err)
	}

	err = client.Ping(context.TODO(), nil)

	if err != nil {
		fmt.Println(err)
	}

	log.Println("Connected to MongoDB!")

	// Don't forget to logout
	defer c.Logout()
	defer client.Disconnect(context.TODO())

	// List mailboxes
	mailboxes := make(chan *imap.MailboxInfo, 10)
	done := make(chan error, 1)
	go func() {
		done <- c.List("", "*", mailboxes)
	}()

	if err := <-done; err != nil {
		fmt.Println(err)
	}

	// Select INBOX
	_, err = c.Select("INBOX", false)
	if err != nil {
		fmt.Println(err)
	}
	// log.Println("Flags for INBOX:", mbox.Flags)

	criteria := imap.NewSearchCriteria()
	criteria.Since = time.Now().Add(-2 * time.Hour)
	// criteria.Before = time.Date(2020, 10, 23, 12, 0, 0, 0, time.UTC)
	// criteria.Since = time.Date(1984, 11, 5, 0, 0, 0, 0, time.UTC)
	// fmt.Println(criteria.Since)
	// criteria.Before = criteria.Since.Add(24 * time.Hour)
	// fmt.Println(criteria.Before)

	// criteria.Since = time.Now().Add(-1 * time.Hour)
	//criteria.WithoutFlags = []string{"\\Seen"}

	// criteria.Since = time.Now().Add(-1 * time.Hour)

	uids, err := c.Search(criteria)
	if err != nil {
		log.Println(err)
	}

	fmt.Println(len(uids))

	// log.Printf(fmt.Sprint(mbox.Recent))
	// log.Printf(mbox.Name)

	// Get the last 4 messages
	// from := uint32(1)
	// to := mbox.Messages
	// if mbox.Messages > 3 {
	// 	// We're using unsigned integers here, only substract if the result is > 0
	// 	from = mbox.Messages - 3
	// }
	seqset := new(imap.SeqSet)
	seqset.AddNum(uids...)
	//seqset.AddRange(from, to)
	messages := make(chan *imap.Message)
	done = make(chan error, 1)
	go func() {
		// done <- c.Fetch(seqset, []imap.FetchItem{imap.FetchRFC822}, messages)
		var section imap.BodySectionName
		done <- c.Fetch(seqset, []imap.FetchItem{section.FetchItem()}, messages)
	}()
	fmt.Println("Messages")
	fmt.Println(len(messages))

	for msg := range messages {
		for _, r := range msg.Body {
			entity, err := message.Read(r)
			if err != nil {
				fmt.Println(err)
			}
			multiPartReader := entity.MultipartReader()
			fmt.Println(multiPartReader)
			if multiPartReader != nil {
				for e, err := multiPartReader.NextPart(); err != io.EOF; e, err = multiPartReader.NextPart() {
					kind, params, cErr := e.Header.ContentType()
					if cErr != nil {
						fmt.Println(err)
					}

					if kind != "application/pdf" && kind != "application/vnd.openxmlformats-officedocument.wordprocessingml.document" && kind != "application/msword" {
						continue
					}

					log.Printf("Date Received: " + entity.Header.Get("Date") + " From: " + entity.Header.Get("From"))

					whenRec, err := time.Parse("Mon, 2 Jan 2006 15:04:05 -0700", entity.Header.Get("Date"))
					if err != nil {
						fmt.Println("Error while parsing date :", err)
					}

					c, rErr := ioutil.ReadAll(e.Body)
					if rErr != nil {
						fmt.Println(err)
					}

					log.Printf("Dump file %s", params["name"])
					fileName := strconv.Itoa(int(whenRec.Unix())) + "-" + strings.ReplaceAll(params["name"], " ", "-")

					if fErr := ioutil.WriteFile(filesPath+fileName, c, 0777); fErr != nil {
						fmt.Println(err)
					}

					filter := bson.M{
						"isCrawled": true,
						"date":      whenRec,
						"from":      entity.Header.Get("From"),
					}

					update := bson.D{{"$set",
						bson.D{
							{"date", whenRec},
							{"from", entity.Header.Get("From")},
							{"filename", fileName},
							{"isCrawled", true},
						},
					}}

					collection := client.Database(dbName).Collection("profile")
					opts := options.Update().SetUpsert(true)
					updateResult, err := collection.UpdateOne(context.TODO(), filter, update, opts)
					if err != nil {
						fmt.Println(err)
					}
					fmt.Printf("Matched %v documents and updated %v documents.\n", updateResult.MatchedCount, updateResult.ModifiedCount)
					break
				}
			}
		}
	}

	fmt.Println("here i am")

	if err := <-done; err != nil {
		fmt.Println(err)
	}
}
